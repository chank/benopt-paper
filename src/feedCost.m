%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BENOPT (BioENergy OPTimisation model)
%     Copyright (C) 2012-2020 Markus Millinger
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 
% Contact: markus.millinger@ufz.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [s,f]=feedCost(s,f)

for time=1:f.runTime
    f.priceDev(time)            =   (1+s.priceDevFactor)^(time-1);
end

    indexWheat                  =   find(contains(f.feedNames,{'Wheat'}));
    f.wheatIncome               =   f.wheatPriceStart*f.feedYieldFM(:,indexWheat).*f.priceDev'; %/ha

    s.labourCost                =   s.labourCostStart.*f.priceDev;
    s.dieselCost                =   s.dieselCostStart.*f.priceDev;
    f.feedLabourCostHa          =   s.labourCost.*f.feedLabourHa';
    f.feedDieselCostHa          =   s.dieselCost.*f.feedDieselHa';
    f.feedExpensesHa            =   f.feedLabourCostHa+f.feedDieselCostHa+ones(1,s.runTime).*(f.feedMachineFixHa+f.feedMachineVarHa+f.feedServiceCostsHa+f.feedDirectCostsHa)';

    f.wheatProfit               =   f.wheatIncome-f.feedExpensesHa(indexWheat,:)';
    f.feedProfit                =   f.wheatProfit;
    f.feedIncomeHa              =   f.feedExpensesHa+ones(1,s.runTime).*f.feedProfit';

        
    %% Residue price development
    f.resPriceMin(:,time)       =   f.resPriceIniMin(:,1).*f.priceDev(time);
    f.resPriceMax(:,time)       =   f.resPriceIniMax(:,1).*f.priceDev(time);
   
    %% Calculate price categories

    f.feedPrice              =   f.feedIncomeHa./f.feedYieldDM'; %/tDM
    f.feedPriceGJBase            =   f.feedPrice./f.feedDMenergyContent'; % /GJ
    
    for c=1:f.cat
        for b=1:f.numFeed
            f.feedPriceGJ(:,b,c)    =   f.feedPriceGJBase(b,:);
        end
        for b=1:f.numResidue
            if c==1
                f.resPrice(:,b,c)       =   f.resPriceMin(b,:);
            elseif c==2
                f.resPrice(:,b,c)       =   (f.resPriceMin(b,:)+f.resPriceMax(b,:))/2;
            elseif c==3
                f.resPrice(:,b,c)       =   f.resPriceMax(b,:);
            end
        end
            f.powerPrice(:,1,c)     =   s.powerPrice(1,:)./3.6; %/MWh -> Mio/PJ (/GJ)
            f.powerPrice(:,2,c)     =   zeros(s.runTime,1); %
    end
    
    %% Land demand / biomass
    f.landReqGJFuel                 =   (f.feedDMenergyContent.*f.feedYieldDM).^-1; %ha/GJ_feed

end