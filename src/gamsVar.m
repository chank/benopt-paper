%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BENOPT (BioENergy OPTimisation model)
%     Copyright (C) 2012-2020 Markus Millinger
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
% Contact: markus.millinger@ufz.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [gamsTemp,g]=gamsVar(s,f,q,r,scenario,g,techData)


%% Definition of sets
% Price categories
g.cats= strsplit(num2str(1:f.cat));
g.C.name = 'cat';
g.C.type='set';
g.C.uels = {g.cats};

g.I.name = 'tech';
g.I.type='set';
g.I.uels = {s.techNames};

g.J.name = 'market';
g.J.type='set';
g.J.uels = {s.sectorNames};


g.F.name = 'fuel';
g.F.type='set';
g.F.uels = {s.fuelNames};

g.BR.name = 'biomassResidue';
g.BR.type='set';
g.BR.uels = {f.residueNames};

g.BF.name = 'biomassCrop';
g.BF.type='set';
g.BF.uels = {f.feedNames};

% Power sources
g.PwrSrc.name = 'powerSource';
g.PwrSrc.type='set';
g.PwrSrc.uels = f.powerNames;

% Power mix set
g.PwrMix.name = 'PowerMix';
g.PwrMix.type='set';
g.PwrMix.uels = {'PowerMix'};

g.PwrRes.name = 'PowerRes';
g.PwrRes.type='set';
g.PwrRes.uels = {'PowerRes'};

% Biomass/Feestock: Residues+Cultivation+power source combined
g.B.name = 'biomass';
g.B.type ='set';
g.B.uels = {f.biomassNames};


%% Set Dependency

% Which technologies use which biomass
g.techbiomass                 =   transpose(techData.data.techInputData(96:(96+f.numResidue+f.numFeed+f.numPowerSource-1),1:s.numTech));
g.techbiomass(isnan(g.techbiomass))=0;

g.tb.uels = {s.techNames,f.biomassNames};
g.tb.name = 'TB';
g.tb.type='set';
g.tb.form='full';
g.tb.val=g.techbiomass;

% % Which technologies are allowed on which sectors (TS(techNames2,sectorNames))
%%
g.techsectors                 =   transpose(techData.data.techInputData(123:133,1:s.numTech));
g.techsectors(isnan(g.techsectors))=0;

g.TS=cell(1,s.numSectors);
for j=1:s.numSectors
    g.TS{j}=transpose(find(g.techsectors(:,j)==1));
end

[row,cols]=find(g.techsectors(:,6:9)==1);
g.TStrans=transpose(unique(row));

[row,cols]=find(g.techsectors(:,1:9)==1);
g.TSall=transpose(unique(row));

[row,cols]=find(g.techsectors(:,1)==1);
g.TSindTH=transpose(unique(row));

[row,cols]=find(g.techsectors(:,2)==1);
g.TSghdTH=transpose(unique(row));

[row,cols]=find(g.techsectors(:,3)==1);
g.TShhTH=transpose(unique(row));

[row,cols]=find(s.powerByprodInit(:)==1);
g.TSconvEL=transpose(unique(row));

[row,cols]=find(g.techsectors(:,4)==1);
g.TSconvELgrid=transpose(unique(row));

[row,cols]=find(g.techsectors(:,5)==1);
g.TSconvTH=transpose(unique(row));

g.ts.uels = {s.techNames,s.sectorNames};
g.ts.name = 'TS';
g.ts.type='set';
g.ts.form='full';
g.ts.val=g.techsectors;

% Which technologies are allowed on which fuel types (TS(techNames2,sectorNames))
%%
g.fuelTypeDef                 =   transpose(techData.data.techInputData(134:140,1:s.numTech));
g.fuelTypeDef(isnan(g.fuelTypeDef))=0;

g.fuelType.uels   =   {s.techNames,s.fuelNames};
g.fuelType.name   =   'fuelType';
g.fuelType.type   =   'set';
g.fuelType.form   =   'full';
g.fuelType.val    =   g.fuelTypeDef;

g.tDiesel.name = 'techDiesel';
g.tDiesel.type='set';
g.tDiesel.uels = {s.techNames(g.fuelTypeDef(:,1)==1)};

g.tEtOH.name = 'techEtOH';
g.tEtOH.type='set';
g.tEtOH.uels = {s.techNames(g.fuelTypeDef(:,2)==1)};

g.tCH4.name = 'techCH4fuel';
g.tCH4.type='set';
g.tCH4.uels = {s.techNames(g.fuelTypeDef(:,3)==1)};

g.tLNG.name = 'techLNG';
g.tLNG.type='set';
g.tLNG.uels = {s.techNames(g.fuelTypeDef(:,4)==1)};

g.tH2.name = 'techH2';
g.tH2.type='set';
g.tH2.uels = {s.techNames(g.fuelTypeDef(:,5)==1)};

g.tAviationFuel.name = 'techAviationFuel';
g.tAviationFuel.type='set';
g.tAviationFuel.uels = {s.techNames(g.fuelTypeDef(:,6)==1)};

g.tEV.name = 'techEV';
g.tEV.type='set';
g.tEV.uels = {s.techNames(g.fuelTypeDef(:,7)==1)};
%%
% % Which technologies belong to which aggregated sector? (TSagg(techNames,sectorAggNames))

g.techsectorsAgg              =   transpose(techData.data.techInputData(141:143,1:s.numTech));
g.techsectorsAgg(isnan(g.techsectorsAgg))=0;


ghgRef                          =   q.referenceGHGemission.*[ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    ones(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)']';

gamsTemp.costMarg           =  gamsVarCreate2('costMarg',q.fuelMargCost,s.year,s.techNames,'parameter'); %M�/PJ
gamsTemp.costInv            =  gamsVarCreate2('costInv',q.plantInvCost,s.year,s.techNames,'parameter'); %M�/GW
gamsTemp.costInvLevel       =  gamsVarCreate2('costInvLevel',q.plantInvCostLevel,s.year,s.techNames,'parameter'); %M�/GW
gamsTemp.ghgEmisFeed        =  gamsVarCreate2('ghgEmisFeed',r.ghgCultivationTotGJfeed,s.year,f.feedNames,'parameter'); %kgCo2eq/GJ = ktCo2eq/PJ
gamsTemp.ghgEmisGateWheel   =  gamsVarCreate2('ghgEmisGateWheel',q.fuelGHGemission,s.year,s.techNames,'parameter'); %kgCo2eq/GJ = ktCo2eq/PJ
gamsTemp.ghgRef             =  gamsVarCreate2('ghgRef',ghgRef,s.year,s.sectorNames,'parameter'); %kgCo2eq/GJ = ktCo2eq/PJ
%     gamsTemp.powerMixEmis       =  gamsVarCreate2('powerMixEmis',q.ghgEFPower./(3.6/1000),s.year,0,'parameter'); %kgCO2eq/kWh * [kWh/GJ]^-1 => kgCO2eq/GJ = ktCO2eq/PJ
%     gamsTemp.ghgEmisT1      =  gamsVarCreate2('ghgEmisT1',r.ghgTransport1GJfeed(:,1),s.year,0,'parameter'); %kgCo2eq/GJ = ktCo2eq/PJ %Not yet feedstock specific

gamsTemp.residualLoad.uels = {strsplit(num2str(g.timeStepsIntraYear)),strsplit(num2str(s.year))};
gamsTemp.residualLoad.name = 'residualLoad';
gamsTemp.residualLoad.type = 'parameter';
gamsTemp.residualLoad.form = 'full';
gamsTemp.residualLoad.val           =   q.surplusPowerVar.*3.6;%g.residualLoad3.*3.6;       %PJ/timestep

% Positive residual load = demand for dispatchable power
gamsTemp.posResLoad.uels = {strsplit(num2str(g.timeStepsIntraYear)),strsplit(num2str(s.year))};
gamsTemp.posResLoad.name = 'posResLoad';
gamsTemp.posResLoad.type = 'parameter';
gamsTemp.posResLoad.form = 'full';
gamsTemp.posResLoad.val           =   q.posResLoad.*3.6;%g.residualLoad3.*3.6;       %PJ/timestep

% Conversion efficiency
gamsTemp.convEta.uels = {strsplit(num2str(s.year)), s.techNames};
gamsTemp.convEta.name = 'convEta';
gamsTemp.convEta.type = 'parameter';
gamsTemp.convEta.form = 'full';
gamsTemp.convEta.val  = q.plantConvEta;     %PJ_energy per PJ_crop

%GHG emissions transport to gate
gamsTemp.ghgEmisT1.uels = {strsplit(num2str(s.year))}; %ADD! , f.feedNames
gamsTemp.ghgEmisT1.name = 'ghgEmisT1';
gamsTemp.ghgEmisT1.type = 'parameter';
gamsTemp.ghgEmisT1.form = 'full';
gamsTemp.ghgEmisT1.val           =   r.ghgTransport1GJfeed(:,1);       %kgCo2eq/GJ = ktCo2eq/PJ

% Conversion efficiency biomass specific
gamsTemp.convEtaBiomSpec.uels = {strsplit(num2str(s.year)), f.biomassNames, s.techNames};
gamsTemp.convEtaBiomSpec.name = 'convEtaBiomSpec';
gamsTemp.convEtaBiomSpec.type = 'parameter';
gamsTemp.convEtaBiomSpec.form = 'full';
gamsTemp.convEtaBiomSpec.val  = q.convEtaBiomSpec;     %PJ_energy per PJ_crop

% Capacity factor
gamsTemp.capF.uels = {strsplit(num2str(s.year)), s.techNames};
gamsTemp.capF.name = 'capF';
gamsTemp.capF.type = 'parameter';
gamsTemp.capF.form = 'full';
gamsTemp.capF.val  = q.plantCapacityFactor;     % dimensionless - full load hours per year divided by hours/year

% Capacity of initial stock for first years of lifetime
gamsTemp.cap0.uels = {strsplit(num2str(s.year)), s.techNames};
gamsTemp.cap0.name = 'cap0';
gamsTemp.cap0.type = 'parameter';
gamsTemp.cap0.form = 'full';
gamsTemp.cap0.val  = q.cap0.*10^-3;%[GW]6./q.plantCapacityFactor; %[PJ]

% Demand
gamsTemp.demand.uels = {strsplit(num2str(s.year)), s.sectorNames};
gamsTemp.demand.name = 'demand';
gamsTemp.demand.type = 'parameter';
gamsTemp.demand.form = 'full';
gamsTemp.demand.val  = g.Demand';  %[PJ]

% Passenger road vehicle km minus EVs
gamsTemp.vehicleKMroadTot.uels = {strsplit(num2str(s.year))};
gamsTemp.vehicleKMroadTot.name = 'vehicleKMroadTot';
gamsTemp.vehicleKMroadTot.type = 'parameter';
gamsTemp.vehicleKMroadTot.form = 'full';
gamsTemp.vehicleKMroadTot.val  = s.passengerVehicleKMtot.*10^-9;  %[Mrd. vehicle-km]

% Passenger road fuel economy baseline
gamsTemp.MJperKMavgICEV.uels = {strsplit(num2str(s.year))};
gamsTemp.MJperKMavgICEV.name = 'MJperKMavgICEV';
gamsTemp.MJperKMavgICEV.type = 'parameter';
gamsTemp.MJperKMavgICEV.form = 'full';
gamsTemp.MJperKMavgICEV.val  = s.MJperKMavgICEV;  %PJ/Mrd. km for average ICEV

% relative fuel economy of fuel types
gamsTemp.relativeFuelEconomy.uels = {s.techNames, s.sectorNames};
gamsTemp.relativeFuelEconomy.name = 'relativeFuelEconomy';
gamsTemp.relativeFuelEconomy.type = 'parameter';
gamsTemp.relativeFuelEconomy.form = 'full';
gamsTemp.relativeFuelEconomy.val  = s.relativeFuelEconomy;  %[frac compared to reference in each sector -> GJ/vehicle-km or tonne-km]

% Historic Vehicle Fuel Demand
gamsTemp.historicFuelDemand.uels = {strsplit(num2str(s.year)), s.fuelNames(1:3)};
gamsTemp.historicFuelDemand.name = 'historicFuelDemand';
gamsTemp.historicFuelDemand.type = 'parameter';
gamsTemp.historicFuelDemand.form = 'full';
gamsTemp.historicFuelDemand.val  =   s.historicICEVPJPerFuelType;%Normed;  %[PJ]

%New ICEV fuel demand per year (PJ)
gamsTemp.newVehicSharePass.uels = {strsplit(num2str(s.year))};
gamsTemp.newVehicSharePass.name = 'newVehicSharePass';
gamsTemp.newVehicSharePass.type = 'parameter';
gamsTemp.newVehicSharePass.form = 'full';
gamsTemp.newVehicSharePass.val  = s.newVehicSharePass;  %share of vehicles

% Power price
gamsTemp.powerPrice.uels = {strsplit(num2str(s.year)), s.techNames};
gamsTemp.powerPrice.name = 'powerPrice';
gamsTemp.powerPrice.type = 'parameter';
gamsTemp.powerPrice.form = 'full';
gamsTemp.powerPrice.val  = 10^-6.*(g.techsectorsAgg*s.powerPrice)';  %Mio �/MWh

% Heat price
gamsTemp.heatPrice.uels = {strsplit(num2str(s.year))};
gamsTemp.heatPrice.name = 'heatPrice';
gamsTemp.heatPrice.type = 'parameter';
gamsTemp.heatPrice.form = 'full';
gamsTemp.heatPrice.val  = 10^-6.*s.heatPrice';  %Mio �/MWh

% Residue import maximum
gamsTemp.resImportMax.uels = {strsplit(num2str(s.year))};
gamsTemp.resImportMax.name = 'resImportMax';
gamsTemp.resImportMax.type = 'parameter';
gamsTemp.resImportMax.form = 'full';
gamsTemp.resImportMax.val  = g.resImportMax';  %PJ_biomass



% Land demand [ha/PJ]
gamsTemp.landDmdPJ.uels = {strsplit(num2str(s.year)), f.feedNames};
gamsTemp.landDmdPJ.name = 'landDmdPJ';
gamsTemp.landDmdPJ.type = 'parameter';
gamsTemp.landDmdPJ.form = 'full';
gamsTemp.landDmdPJ.val  = r.landReqGJFuel.*10^6;

% Maximal available land for cultivation
gamsTemp.landMax.uels   =   {strsplit(num2str(s.year))};
gamsTemp.landMax.name   =   'landMax';
gamsTemp.landMax.type   =   'parameter';
gamsTemp.landMax.form   =   'full';
gamsTemp.landMax.val    =   g.landMax;
% land max value: see switch command in loop


% land use initial [ha]
gamsTemp.feedLandUseInit.uels   =   {f.feedNames};
gamsTemp.feedLandUseInit.name   =   'feedLandUseInit';
gamsTemp.feedLandUseInit.type   =   'parameter';
gamsTemp.feedLandUseInit.form   =   'full';
gamsTemp.feedLandUseInit.val    =   f.feedLandUseInit'; %[ha]


% lifetime of technologies
gamsTemp.lifeT.uels   =   {s.techNames};
gamsTemp.lifeT.name   =   'lifeT';
gamsTemp.lifeT.type   =   'parameter';
gamsTemp.lifeT.form   =   'full';
gamsTemp.lifeT.val    =   q.plantLifeTime';

% power input of technologies
gamsTemp.powerInput.uels   =   {s.techNames};
gamsTemp.powerInput.name   =   'powerInput';
gamsTemp.powerInput.type   =   'parameter';
gamsTemp.powerInput.form   =   'full';
gamsTemp.powerInput.val    =   10^3.*q.powerInput'; %MWh/PJ

% heat input of technologies
gamsTemp.heatInput.uels   =   {s.techNames};
gamsTemp.heatInput.name   =   'heatInput';
gamsTemp.heatInput.type   =   'parameter';
gamsTemp.heatInput.form   =   'full';
gamsTemp.heatInput.val    =   10^3.*q.heatInput'; %MWh/PJ

% Potential of biomass residues
gamsTemp.bioResPot.uels = {strsplit(num2str(s.year)), f.residueNames};
gamsTemp.bioResPot.name = 'bioResPot';
gamsTemp.bioResPot.type = 'parameter';
gamsTemp.bioResPot.form = 'full';
gamsTemp.bioResPot.val  = f.resPot';

% Potential of biomass residue import
gamsTemp.bioResPotImport.uels = {strsplit(num2str(s.year)), f.biomassNames};
gamsTemp.bioResPotImport.name = 'bioResPotImport';
gamsTemp.bioResPotImport.type = 'parameter';
gamsTemp.bioResPotImport.form = 'full';
gamsTemp.bioResPotImport.val  = f.resPotImport';

% Biomass price (time, biomass, cats) [Mio�/PJ]
gamsTemp.biomassPrice.uels = {strsplit(num2str(s.year)), f.biomassNames, g.cats};
gamsTemp.biomassPrice.name = 'biomassPrice';
gamsTemp.biomassPrice.type = 'parameter';
gamsTemp.biomassPrice.form = 'full';
gamsTemp.biomassPrice.val  = [f.feedPriceGJ f.resPrice f.powerPrice]; %[Mio�/PJ]

% Biomass price Import (time, biomass) [Mio�/PJ]
gamsTemp.biomassPriceImport.uels = {strsplit(num2str(s.year)), f.biomassNames};
gamsTemp.biomassPriceImport.name = 'biomassPriceImport';
gamsTemp.biomassPriceImport.type = 'parameter';
gamsTemp.biomassPriceImport.form = 'full';
gamsTemp.biomassPriceImport.val  = squeeze(gamsTemp.biomassPrice.val(:,:,3));% + f.ImportPriceSur';



% Process CO2 feedstock input (PtX)
gamsTemp.CO2input.uels   =   {s.techNames};
gamsTemp.CO2input.name   =   'CO2input';
gamsTemp.CO2input.type   =   'parameter';
gamsTemp.CO2input.form   =   'full';
gamsTemp.CO2input.val    =   s.feed2ndCO2Amount; %t/GJ=Mt/PJ

% CO2 feedstock price
gamsTemp.CO2price.uels   =   {strsplit(num2str(s.year))};
gamsTemp.CO2price.name   =   'CO2price';
gamsTemp.CO2price.type   =   'parameter';
gamsTemp.CO2price.form   =   'full';
gamsTemp.CO2price.val    =   s.co2price;

% Maximal available CO2 as input
gamsTemp.CO2source.uels   =   {strsplit(num2str(s.year))};
gamsTemp.CO2source.name   =   'CO2source';
gamsTemp.CO2source.type   =   'parameter';
gamsTemp.CO2source.form   =   'full';
gamsTemp.CO2source.val    =   q.co2source;

gamsTemp.ghgRefCO2.name = 'ghgRefCO2';
gamsTemp.ghgRefCO2.type = 'parameter';
gamsTemp.ghgRefCO2.val  = s.ghgRefCO2; %MtCO2/MtCO2

gamsTemp.pwrMixMax.name = 'pwrMixMax';
gamsTemp.pwrMixMax.type = 'parameter';
gamsTemp.pwrMixMax.val  = s.powerMixMax; %PJ

% Process H2 feedstock input (PtX)
gamsTemp.H2input.uels   =   {strsplit(num2str(s.year)), s.techNames};
gamsTemp.H2input.name   =   'H2input';
gamsTemp.H2input.type   =   'parameter';
gamsTemp.H2input.form   =   'full';
gamsTemp.H2input.val    =   s.feed2ndH2in; %GJ/GJ

%GHG target
gamsTemp.ghgTarget.name = 'ghgTarget';
gamsTemp.ghgTarget.type = 'parameter';
%     gamsTemp.ghgTarget.form = 'full';

%Time steps intra-year
g.d.name = 'd';
g.d.type = 'set';
g.d.uels = {g.timeStepsIntraYear};

gamsTemp.dMax.name = 'dMax';
gamsTemp.dMax.type = 'parameter';
gamsTemp.dMax.val  = max(g.timeStepsIntraYear);


% Power mix GHG emissions
gamsTemp.powerMixEmis.uels = {strsplit(num2str(s.year))};
gamsTemp.powerMixEmis.name = 'powerMixEmis';
gamsTemp.powerMixEmis.type = 'parameter';
gamsTemp.powerMixEmis.form = 'full';
gamsTemp.powerMixEmis.val  = q.ghgEFPower./(3.6/1000); %kgCO2eq/kWh * [kWh/GJ]^-1 => kgCO2eq/GJ = ktCO2eq/PJ

% Goal function
gamsTemp.solveOption.name = 'solveOption';
gamsTemp.solveOption.type = 'parameter';

% Heat byproduct (time, tech) [PJ/PJ]
gamsTemp.heatByprod.uels = {strsplit(num2str(s.year)), s.techNames};
gamsTemp.heatByprod.name = 'heatByprod';
gamsTemp.heatByprod.type = 'parameter';
gamsTemp.heatByprod.form = 'full';
heatByprodTemp=q.heatByprod;
heatByprodTemp(heatByprodTemp==0)=1; % replace 0 with 1 -> now one can easily switch the byproducts in GAMS
gamsTemp.heatByprod.val  = heatByprodTemp;

%H2 max in all sectors
gamsTemp.H2Max.uels     = {strsplit(num2str(s.year)), s.sectorNames};
gamsTemp.H2Max.name     = 'H2Max';
gamsTemp.H2Max.type     = 'parameter';
gamsTemp.H2Max.form     = 'full';
gamsTemp.H2Max.val      = [zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    ones(s.runTime,1)';
    g.H2maxAviation;
    g.H2maxGoods;
    g.H2maxShipping;
    ones(s.runTime,1)';
    ones(s.runTime,1)']';

%CH4 max in all sectors
gamsTemp.CH4Max.uels     = {strsplit(num2str(s.year)), s.sectorNames};
gamsTemp.CH4Max.name     = 'CH4Max';
gamsTemp.CH4Max.type     = 'parameter';
gamsTemp.CH4Max.form     = 'full';
gamsTemp.CH4Max.val      = [zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    g.CH4maxLand;
    zeros(s.runTime,1)';
    g.CH4maxGoods;
    g.CH4maxShipping;
    ones(s.runTime,1)';
    ones(s.runTime,1)']';

%LCH4 max in all sectors
gamsTemp.LCH4Max.uels     = {strsplit(num2str(s.year)), s.sectorNames};
gamsTemp.LCH4Max.name     = 'LCH4Max';
gamsTemp.LCH4Max.type     = 'parameter';
gamsTemp.LCH4Max.form     = 'full';
gamsTemp.LCH4Max.val      = [zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    zeros(s.runTime,1)';
    g.LCH4maxLand;
    zeros(s.runTime,1)';
    g.LCH4maxGoods;
    g.LCH4maxShipping;
    ones(s.runTime,1)';
    ones(s.runTime,1)']';


end