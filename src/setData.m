%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BENOPT (BioENergy OPTimisation model)
%     Copyright (C) 2012-2020 Markus Millinger
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 
% Contact: markus.millinger@ufz.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Function for setting the data
%

function [f,g,s] = setData(techData)

s.numTech                   =   size(techData.data.techInputData(1,:),2);
s.techNames                 =   techData.textdata.techInputData(2,3:s.numTech+2);
s.techNamesLegend           =   s.techNames;
s.numSectors                =   11;
s.sectorNames               =   techData.textdata.techInputData(125:135,2)';
s.sectorNamesLegend         =   s.sectorNames;
s.fuelNames                 =   techData.textdata.techInputData(136:142,2)';
f.numResidue                =   12;
f.residueNames              =   techData.textdata.biomassResPot(2:f.numResidue+1,1)';
f.numFeed                   =   size(techData.data.feedstockInputData(1,1:end),2);
f.feedNames                 =   techData.textdata.feedstockInputData(1,3:f.numFeed+2);

f.cat                       =   3;
f.numPowerSource            =   2;
f.powerNames                =   {'PowerMix','PowerRes'};

f.biomassNames              =   [f.feedNames f.residueNames f.powerNames];

%% Definition of Input Data s= Technology specific  f=feedstock specific

%Discount rate
s.discountRateInvest        =   0.05;

s.inflation                 =   0;
s.plantPayBackTime          =   20;
s.dieselCostStart           =   0.9; %�/l
s.labourCostStart           =   15; %�/h
s.referenceGHGemission      =   94.1; %kgCO2/GJ

f.wheatPriceStart           =   189;% %�/tFM
f.rapeSeedPriceStart        =   381;% %�/tFM

s.runTime                   =   2051-2020;
f.runTime                   =   s.runTime;
s.year                      =   linspace(2020,2050,s.runTime);
s.hourYear                  =   linspace(1,8760,8760);
s.dayYear                   =   linspace(1,365,365);


    %Set parameters for power sector
    s.RENMustRun                    =   2483; %Hydro average over year
    s.MustRun                       =   0; %Must-run electriticy generation capacity
    g.timeStepsIntraYear            =   1:50; %up to 8760 possible (heavy on run-time)
    s.onShore                       =   round(linspace(54,170,7)); %GW capacity from start year to end year in 5-year steps
    s.offShore                      =   round(linspace(7.5,54,7)); %GW
    s.photoV                        =   round(linspace(49,200,7)); %GW
    s.demandPower                   =   round(linspace(513,700,7)); %GW
    s.powerStorage                  =   linspace(9000,30000,7);     %GW
    s.powerStorageMax               =   linspace(66000,100000,7);   %GWh
 
    %Set parameters for crops
    s.priceDevFactor                =   0.04; % frac/a
    g.landMax                       =   10^6.*linspace(1,1,s.runTime);   %ha
    
    %Set restrictions for fuel types in sectors
    %--Hydrogen
    g.H2maxShipping                 =   linspace(0,0.1,s.runTime);    %Share of market
    g.H2maxGoods                    =   linspace(0,0.3,s.runTime);    %Share of market
    g.H2maxAviation                 =   linspace(0,0.1,s.runTime);    %Share of market
    
    %--Methane
    g.CH4maxLand                    =   linspace(0,0,s.runTime);   %Share of market
    g.CH4maxShipping                =   linspace(0,0,s.runTime);   %Share of market
    g.CH4maxGoods                   =   linspace(0,0.3,s.runTime);    %Share of market
    
    %--Liquefied Methane
    g.LCH4maxLand                   =   linspace(0,0,s.runTime);   %Share of market
    g.LCH4maxShipping               =   linspace(0,0.3,s.runTime);   %Share of market
    g.LCH4maxGoods                  =   linspace(0,0.3,s.runTime);    %Share of market
    
    %Set input CO2 limitations, price and GHG reference for CO2 usage
    s.co2source                     =   linspace(60,60,s.runTime);
    s.co2price                      =   50*linspace(1,1,s.runTime); %�/tCO2
    s.ghgRefCO2                     =   0;        

    %Maximal usage of power mix for EVs or hydrogen (if power mix is
    %checked in the input file for these usages)
    s.powerMixMax                   =   70*3.6; %PJ

%% Demand (time,sectors) [PJ]
g.DemandData            =   techData.data.demandData(58:57+5,[1 4 9 14 19 24 29 34]);
% g.DemandData95proc            =   techData.data.otherInputData95proc(58:57+5,[1 34]);
for j=1:5
    g.Demand(j,:)       =   interp1([2017 2020 2025 2030 2035 2040 2045 2050],g.DemandData(j,:),2020:1:2050,'linear')*3.6;
%     g.Demand95proc(j,:)       =   interp1([2017 2050],g.DemandData95proc(j,:),2020:1:2050,'linear')*3.6;
end

%% System data
%Power price & heat price for required input
s.powerPrice                =   10^3.*10^-2.*techData.data.demandData(10:12,1:s.runTime);%[�/MWh]
s.powerPriceDevNormed       =   techData.data.demandData(8,1:s.runTime);%[�/MWh]
s.heatPrice                 =   10^3.*10^-2.*techData.data.demandData(15,1:s.runTime)';%[�/MWh]
%- for process heat, see feedCost function. Possibly differentiate input and output heat price - district heating could be output heat price reference
s.GHGabateTarget            =   160*10^9*linspace(0.05,0.08,s.runTime); %kgCO2eq/a

%% Definition of technology data / Life Time

% Technology Lifetime (tech) [a]
s.plantLifeTime             =   techData.data.techInputData(1,1:s.numTech); %a

%% Definition of initial capacity
s.plantCap             =   techData.data.techInputData(7,1:s.numTech);%*10^6;

%% Definition of investment

% Technology full load hours per year (tech) [h/a]
s.fullLoadHoursInit         =   techData.data.techInputData(2,1:s.numTech); %h/a
s.fullLoadHoursEnd          =   techData.data.techInputData(3,1:s.numTech); %h/a
% Technology Invest(time) [�/MWcap]
s.plantInvCostProMWinit     =   techData.data.techInputData(4,1:s.numTech); %�/kWcap  = M�/GWcap
s.plantInvCostProMWend      =   techData.data.techInputData(5,1:s.numTech); %�/kWcap = M�/GWcap
% Maint & Operation [%/invest)
s.plantMOInvShareInit       =   techData.data.techInputData(9,1:s.numTech)*10^-2;
% s.plantMOInvShareEnd        =   techData.data.techInputData(10,1:s.numTech)*10^-2;
% Labour costs
s.plantOperLaborCost        =   techData.data.techInputData(10,1:s.numTech); %�/GJ

%% Calculation of conversion efficency over time and capacity factor

s.plantConvEtaInit          =   techData.data.techInputData(11,1:s.numTech); %fraction of energy in feedstock in end product (MJ/MJ)
s.plantConvEtaLimit         =   techData.data.techInputData(12,1:s.numTech);

s.convEtaBiomSpec(1,:,:)            =   techData.data.techInputData(147:170,1:s.numTech);
s.convEtaBiomSpec(s.runTime,:,:)    =   techData.data.techInputData(173:196,1:s.numTech);

for tech=1:s.numTech
    for biomass=1:length(f.biomassNames)
        s.convEtaBiomSpec(:,biomass,tech)           =   linspace(s.convEtaBiomSpec(1,biomass,tech),s.convEtaBiomSpec(s.runTime,biomass,tech),s.runTime);
    end
    s.plantConvEta(:,tech)                          =   linspace(s.plantConvEtaInit(tech),s.plantConvEtaLimit(tech),s.runTime)';
    s.plantFullLoadHours(:,tech)                    =   linspace(s.fullLoadHoursInit(tech),s.fullLoadHoursEnd(tech),s.runTime)';
    s.plantMOInvShare(:,tech)                       =   linspace(s.plantMOInvShareInit(tech),s.plantMOInvShareInit(tech),s.runTime)';
end

s.plantCapacityFactor       =   s.plantFullLoadHours./8760;

%% Definition of Biomass/ Feedstock data

%%%%%%%% GHG Feedstock
%Emissions
f.ghgSeeds                  =   techData.data.feedstockInputData(19,1:end); %kg/ha/a
f.ghgN2OhaAverage           =   techData.data.feedstockInputData(20,1:f.numFeed); %kg N2O/ha/a
f.ghgN2OhaHigh              =   techData.data.feedstockInputData(21,1:f.numFeed); %kg N2O/ha/a
f.ghgN2OhaLow               =   techData.data.feedstockInputData(22,1:f.numFeed); %kg N2O/ha/a
f.ghgN2Oha                  =   f.ghgN2OhaAverage;

f.ghgNha                    =   techData.data.feedstockInputData(23,1:f.numFeed); %kg N/ha/a
f.ghgDiesel                 =   techData.data.feedstockInputData(30,1:f.numFeed); %l/ha/a

f.ghgP2O5                   =   techData.data.feedstockInputData(24,1:f.numFeed); %kg/ha/a
f.ghgK2O                    =   techData.data.feedstockInputData(25,1:f.numFeed); %kg/ha/a
f.ghgCaO                    =   techData.data.feedstockInputData(26,1:f.numFeed); %kg/ha/a
f.ghgMgO                    =   techData.data.feedstockInputData(27,1:f.numFeed); %kg/ha/a
f.ghgPesticides             =   techData.data.feedstockInputData(28,1:f.numFeed); %kg/ha/a
f.ghgPowerDrying            =   techData.data.feedstockInputData(29,1:f.numFeed); %kWh/ha/a

% Emission factors
f.ghgEFSeeds                =   techData.data.feedstockInputData(36,1:f.numFeed); %kg/ha/a
f.ghgEFN2O                  =   298; %CO2eq/kg N2O
f.ghgEFNStandard            =   5.88; %CO2eq/kg N
f.ghgEFP2O5                 =   1.01; %/kg
f.ghgEFK2O                  =   [0.58 0.66]; %/kg
f.ghgEFCaO                  =   [0.13 0.89]; %/kg
f.ghgEFMgO                  =   0; %/kg
f.ghgEFPesticides           =   [10.97 13.9]; %/kg

% Energy content
f.feedDMcontent             =   techData.data.feedstockInputData(1,1:f.numFeed); %frac
f.feedDMenergyContent       =   techData.data.feedstockInputData(2,1:f.numFeed); %GJ/tDM
f.feedFMenergyContent       =   f.feedDMenergyContent.*f.feedDMcontent; %GJ/tFM

s.feedFMenergyContent       =   techData.data.techInputData(25,1:s.numTech); %GJ/tFM

f.feedYieldFMlow            =   techData.data.feedstockInputData(3,1:f.numFeed); %GJ/tFM
f.feedYieldFMhigh           =   techData.data.feedstockInputData(4,1:f.numFeed); %GJ/tFM
for i=1:f.numFeed
    f.feedYieldFM(:,i)               =   linspace(f.feedYieldFMlow(i),f.feedYieldFMhigh(i),s.runTime);
end
    f.feedYieldDM               =   f.feedYieldFM.*f.feedDMcontent;
    f.feedYieldGJ               =   f.feedYieldDM.*f.feedDMenergyContent;
    f.feedFMenergyContent       =   f.feedDMcontent.*f.feedDMenergyContent;

f.feedLabourHa         =   techData.data.feedstockInputData(6,1:f.numFeed); %Arbeitskraftstunden/ha
f.feedDieselHa         =   techData.data.feedstockInputData(7,1:f.numFeed); %Dieselbedarf/ha
f.feedMachineFixHa     =   techData.data.feedstockInputData(8,1:f.numFeed);
f.feedMachineVarHa     =   techData.data.feedstockInputData(9,1:f.numFeed);
f.feedServiceCostsHa    =   techData.data.feedstockInputData(10,1:f.numFeed);
f.feedDirectCostsHa    =   techData.data.feedstockInputData(11,1:f.numFeed);

f.feedLandUseInit          =  techData.data.feedstockInputData(12,f.numFeed); % [ha]

% Residue potential data [PJ_feed]
f.resPot                    =   techData.data.biomassResPot(2:1+f.numResidue,1:s.runTime);
f.resPotImport              =   techData.data.biomassResPot(31:31+f.numResidue+f.numFeed-1,1:s.runTime);

% Residue price start [�/GJ]
f.resPriceIniMin            =   techData.data.biomassResPot(17:16+f.numResidue,1);
f.resPriceIniMax            =   techData.data.biomassResPot(17:16+f.numResidue,2);

g.resImportMaxIn            =   techData.data.biomassResPot(60,[1 4 14 24 34]);
g.resImportMax              =   interp1([2017 2020 2030 2040 2050],g.resImportMaxIn,2020:1:2050,'linear');



%% Definition of technology data
s.heatInput                 =   techData.data.techInputData(16,1:s.numTech); %kWh/GJ
s.powerInput                =   techData.data.techInputData(17,1:s.numTech); %kWh/GJ

s.transportCostLow          =   techData.data.techInputData(18,1:s.numTech); %EUR/GJ
s.transportCostHigh         =   techData.data.techInputData(19,1:s.numTech); %EUR/GJ
s.storageCostLow            =   techData.data.techInputData(20,1:s.numTech); %EUR/GJ
s.storageCostHigh           =   techData.data.techInputData(21,1:s.numTech); %EUR/GJ

for i=1:s.numTech
    s.transportCost(i,:)    =   linspace((s.transportCostHigh(i)-s.transportCostLow(i))/2,s.transportCostLow(i),s.runTime);
    s.storageCost(i,:)      =   linspace((s.storageCostHigh(i)-s.storageCostLow(i))/2,s.storageCostLow(i),s.runTime);
end

s.feed2ndMethanolAmount     =   techData.data.techInputData(34,1:s.numTech); %t/GJ
s.feed2ndH2inLow            =   techData.data.techInputData(35,1:s.numTech); %t/GJ
s.feed2ndH2inHigh           =   techData.data.techInputData(36,1:s.numTech); %t/GJ
s.feed2ndCO2Amount          =   techData.data.techInputData(37,1:s.numTech); %t/GJ

s.byProdDigestate           =   techData.data.techInputData(40,1:s.numTech); %t/GJ
s.byProdVinasse             =   techData.data.techInputData(41,1:s.numTech); %t/GJ
s.byProdDriedPulp           =   techData.data.techInputData(42,1:s.numTech); %t/GJ
s.byProdAldehyde            =   techData.data.techInputData(43,1:s.numTech); %t/GJ
s.byProdDDGS                =   techData.data.techInputData(44,1:s.numTech); %t/GJ
s.byProdSchrot              =   techData.data.techInputData(45,1:s.numTech); %t/GJ
s.byProdPharmaglycerin      =   techData.data.techInputData(46,1:s.numTech); %t/GJ

% Heat as Byprod
s.heatByprodInit            =   techData.data.techInputData(47,1:s.numTech); %GJ/GJ
s.heatByprodLimit           =   techData.data.techInputData(48,1:s.numTech); %GJ/GJ

s.byProdNaphtha             =   techData.data.techInputData(49,1:s.numTech); %EUR/GJ

%Power as Byprod
s.powerByprodInit           =   techData.data.techInputData(50,1:s.numTech); %GJ/GJ
s.powerByprodLimit          =   techData.data.techInputData(51,1:s.numTech); %GJ/GJ

% Byproduct interpolate over years
for tech = 1:s.numTech 
    s.heatByprod(:,tech)    =   linspace(s.heatByprodInit(tech),s.heatByprodLimit(tech),s.runTime);
    s.powerByprod(:,tech)   =   linspace(s.powerByprodInit(tech),s.powerByprodLimit(tech),s.runTime);
    s.feed2ndH2in(:,tech)   =   linspace(s.feed2ndH2inHigh(tech),s.feed2ndH2inLow(tech),s.runTime);
end

%%%%%%%%%%%%%%%%%% GHG Processes

s.ghgTransp1Amount          =   24; %t/transport
s.ghgTransp1DistFull        =   80; %km/transport
s.ghgTransp1DistEmpty       =   20; %km/transport
s.ghgTranspDieselFull       =   0.41; %/km
s.ghgTranspDieselEmpty      =   0.24; %/km

s.ghgP1Heat                 =   s.heatInput.*s.feedFMenergyContent.*s.plantConvEta; %kWh/GJ_fuel - conversion efficiency to kWh/tFM
s.ghgP1Power                =   s.powerInput.*s.feedFMenergyContent.*s.plantConvEta; %kWh/GJ_fuel - converted to kWh/tFM
s.ghgP1Hydrogen             =   10^3.*s.feed2ndH2in.*s.feedFMenergyContent.*s.plantConvEta; %kg/tFM
s.ghgP1CO2                  =   10^3.*s.feed2ndCO2Amount.*s.feedFMenergyContent.*s.plantConvEta; %kg/tFM

s.ghgP1AllocationFactor     =   techData.data.techInputData(74,1:s.numTech); %proc
s.ghgP1Heat(:,s.ghgP1AllocationFactor~=1)   =   techData.data.techInputData(72,s.ghgP1AllocationFactor~=1).*linspace(1,1,s.runTime)'; %MJ/t_feed
s.ghgP1Power(:,s.ghgP1AllocationFactor~=1)  =   techData.data.techInputData(73,s.ghgP1AllocationFactor~=1).*linspace(1,1,s.runTime)'; %MJ/t_feed


s.ghgP2AllocationFactor     =   techData.data.techInputData(81,1:s.numTech); %proc
s.ghgP2Heat         =       zeros(s.runTime,s.numTech);
s.ghgP2Power        =       zeros(s.runTime,s.numTech);
s.ghgP2CH3OH        =       zeros(s.runTime,s.numTech);
    s.ghgP2Heat(:,s.ghgP1AllocationFactor~=1)   =   techData.data.techInputData(78,s.ghgP1AllocationFactor~=1).*linspace(1,1,s.runTime)'; %MJ/t_feed_intermediate
    s.ghgP2Power(:,s.ghgP1AllocationFactor~=1)  =   techData.data.techInputData(79,s.ghgP1AllocationFactor~=1).*linspace(1,1,s.runTime)'; %MJ/t_feed_intermediate
    s.ghgP2CH3OH(:,s.ghgP1AllocationFactor~=1)  =   techData.data.techInputData(80,s.ghgP1AllocationFactor~=1).*linspace(1,1,s.runTime)'; %MJ/t_feed_intermediate

s.ghgTranspAmount           =   1+49.*techData.data.techInputData(86,1:s.numTech); %t/transport - either 1 (grid) or 50 (tanker)
s.ghgTranspGasGridPower     =   4.625.*techData.data.techInputData(85,1:s.numTech); %kWh/m� - only for those with grid option marked "1"
s.ghgTranspProcessHeat      =   1.6.*techData.data.techInputData(85,1:s.numTech); %MJ/m� - only for those with grid option marked "1"
s.ghgTransp2DistFull        =   150.*techData.data.techInputData(86,1:s.numTech); %km/transport - only for those with tanker option marked "1"
s.ghgTransp2DistEmpty       =   50.*techData.data.techInputData(86,1:s.numTech); %km/transport - only for those with tanker option marked "1"

s.fuelSpecificEnergy        =   techData.data.techInputData(89,1:s.numTech); %GJ/t [CH4: /m�, unit for electricity?]

s.ghgEFDiesel               =   3.14; %/l
s.ghgEFProcessH2O           =   0.0004; %kgeq/kg
s.ghgEFHNO3                 =   1.89; %kgCO2eq/kg
s.ghgEFNaOH                 =   [0.47 1.12]; %kgCO2eq/kg
s.ghgEFH3PO4                =   3.011; %kgCO2eq/kg
s.ghgEFDryYeast             =   3.2; %kgCO2eq/kg
s.ghgEFCH4N2O               =   0.81; %kgCO2eq/kg

s.ghgEFCH3OH                =   (10^-3)*43*linspace(27,7,s.runTime); %43 GJ/t (BTL) * kgCO2eq/GJ * t/kg => kgCO2eq/kg

f.ghgEFN                        =   linspace(f.ghgEFNStandard,f.ghgEFNStandard*0.2,s.runTime);
s.heatOption                    =   2;
s.heatEta                       =   0.8;
s.ghgEFDiesel                   =   linspace(3.14,3.14*0.2,s.runTime); %/l
s.ghgP2YieldByprod              =   zeros(1,s.numTech);
s.ghgP2ByprodUpgradePower       =   zeros(1,s.numTech);
s.ghgEFPowerData                =   techData.data.powerEmissions(1,[2017 2018 2030 2050]-2016); %kgCO2eq/kWh interp1([2015 2020 2025 2030 2035 2040 2045 2050],co2sourceData,2020:1:2050,'linear');
s.ghgEFPower                    =   interp1([2017 2018 2030 2050],s.ghgEFPowerData,2020:1:2050,'linear');
s.ghgEFHydrogen                 =   0.*(s.ghgEFPower./0.7); %Assumed renewable! %om power mix, with 70% eta
s.ghgEFCO2                      =   zeros(1,s.runTime);

%% VEHICLES
s.relativeFuelEconomy           =   techData.data.techInputData(56:66,1:s.numTech)'; %GJ/GJ, relative to Petrol/Otto process, for diff. sectors
s.vehicleCostPerVehicleStart    =   techData.data.techInputData(67,1:s.numTech); %�/vehicle
s.vehicleCostPerVehicleEnd      =   techData.data.techInputData(68,1:s.numTech); %�/vehicle
% s.vehicleCost                   =   linspace(s.vehicleCostPerVehicleStart,s.vehicleCostPerVehicleEnd,s.runTime);

s.literPetrolPer100KMstart      =   6.1;  %liter petrol per 100 km on average in 2018
s.eDensityPetrol                =   32.7; %MJ per liter petrol
s.MJperKMavgICEVstart           =   s.literPetrolPer100KMstart*s.eDensityPetrol/100; %liter/100km * MJ / liter  /100 => MJ/km
s.vehicleEtaBaselineDev         =   linspace(1,0.6,s.runTime);

s.MJperKMavgICEV                =   s.MJperKMavgICEVstart.*s.vehicleEtaBaselineDev; %MJ/vehicle-km
s.personPerVehicle              =   1.5;

s.personKMtot                   =   10^9*linspace(934,934,s.runTime);%10^9.*[934	938	943	947	952	956	961	965	969	974	978	983	987	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992	992];
s.passengerVehicleKMtot         =   s.personKMtot./s.personPerVehicle;

s.totalVehicles                 =   48000000;

s.vehicleLifeT                  =   14;
s.passengerVehicleKMperVehicle  =   s.passengerVehicleKMtot./s.totalVehicles;

s.newVehiclesPerYear            =     linspace(s.totalVehicles/s.vehicleLifeT,s.totalVehicles/s.vehicleLifeT,s.runTime); %This assumed a constant passenger vehicle fleet
s.newVehicSharePass             =       s.newVehiclesPerYear/s.totalVehicles;
s.EVstart                       =     137000;
s.EVincreaseFactor              =   1.5;
s.EVincreaseFactorLow           =   1.3;
s.EVnewStart                    =   s.EVstart*(s.EVincreaseFactor-1);
s.EVpowerReq                    =   linspace(0.2,0.15,s.runTime);

s.discountRateVehicle                           =   0.05;
s.annuityFactorVehicle                          =   (s.discountRateVehicle*(1+s.discountRateVehicle)^s.vehicleLifeT)/((1+s.discountRateVehicle)^s.vehicleLifeT-1);
    
%The number of historic passenger vehicles in each fuel type
%(Diesel,Hybrid (excl. Plug-in-hybrid),Plug-in-Hybrid,Battery electric/Fuel
%cell,Natural gas (incl. LPG),Flex-Fuel,Petrol)
s.fuelTypePassengerVehicles =   techData.data.vehicleMarket(20:50,14:20);
s.passengerVehiclesTotal    =   sum(s.fuelTypePassengerVehicles,2);
s.fuelTypePassengerVehiclesMainFuels = s.fuelTypePassengerVehicles(:,[1 7 5]);

s.fuelTypeSharePassengerVehicles   =   s.fuelTypePassengerVehicles(:,[1 7 5])./s.totalVehicles;         %Diesel, Petrol, Gas vehicles 2017-2030
s.fuelTypeSharePassengerVehicles(isnan(s.fuelTypeSharePassengerVehicles))=0;

s.sharePassengerICEVsTotal  =   sum(s.fuelTypeSharePassengerVehicles,2);

s.demLand                       =   s.MJperKMavgICEV.*s.passengerVehicleKMtot*10^-9;%
s.historicICEVPJPerFuelType     =   s.fuelTypeSharePassengerVehicles.*s.demLand';
    
    
%% Initial capacity decommissioning
for i=1:s.numTech
    s.cap1(1:s.plantLifeTime(i),i)                  =   linspace(s.plantCap(i),0,s.plantLifeTime(i));
end
    s.cap0                                          =   s.cap1(1:s.runTime,1:s.numTech); %Necessary if plantLifeTime > runTime (otherwise matrix is too large in gamsRun)
end