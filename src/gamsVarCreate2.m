%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BENOPT (BioENergy OPTimisation model)
%     Copyright (C) 2012-2020 Markus Millinger
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 
% Contact: markus.millinger@ufz.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Creation of GAMS variables
%

function gamsVarOut   =   gamsVarCreate2(name,inputVar,x,y,type)

% gamsVarOut  =   gamsVarIn;
% gamsVarOut(end+1)  =   setfield(name,name);

if contains(type,'param')
    if isnumeric(x)
        x   =   strsplit(num2str(x));
    elseif isnumeric(y)
        y   =   num2str(y);
    end
    gamsVarOut.uels = {x, y};
    gamsVarOut.name = name;
    gamsVarOut.type = 'parameter';
    gamsVarOut.form = 'full';
    gamsVarOut.val  = inputVar;

elseif contains(type,'set')
    gamsVarOut.name           =   name;
    gamsVarOut.type           =   'set';
    gamsVarOut.uels           =   {inputVar};
    
elseif contains(type,'scalar')
    gamsVarOut.name = name;
    gamsVarOut.type = 'parameter';
end
end
