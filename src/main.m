%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BENOPT (BioENergy OPTimisation model)
%     Copyright (C) 2012-2020 Markus Millinger
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 
% Contact: markus.millinger@ufz.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Start function for the model
%

clearvars;
close all;

%% Data Import

% Conversion options, biomass crops and residues and other data
% techData                =   importdata('../data/BENOPT_inputData_200409.xls');
% save('../data/techData.mat','techData');
techData                    =   importdata('../data/techData.mat');

%Set NaN data to zero
techData.data.techInputData(isnan(techData.data.techInputData))             =   0;
techData.data.feedstockInputData(isnan(techData.data.feedstockInputData))   =   0;

%Electricity generation base data
powerData                   =   importdata('../data/powerData.mat');

%% Scenario parameter calculation and optimisation in GAMS

noScenarios                 =   2;
for scenario                =   1:noScenarios
   
    %Clear variables for each scenario loop
    %(f=feedstock, g=gams related variables, s=technologies, gamsTemp=gams variables)
    clear matdata.gdx;
    clear gamsTemp;
    clear g;
    clear f;
    clear s;
    
    %Set variables 
    [f,g,s]                         =   setData(techData);
    
    %Calculate GHG emissions
    [s,f]                           =   ghgEmissions(s,f);
    
    %% Monte Carlo sensitivity analysis example of VRE module
                [monteCarloOut,weatherYear,H2,runTimeGAMS,timeSteps,EVfactor,dRate,CO2source,H2max,landMax,CO2Use]...
                =   monteCarlo(1000,s,f,g,techData,powerData);
    
%% Scenario loop
switch scenario
        case 1 %Base scenario
        case 2 % CO2 GHG reference storage and land available decreasing to zero
            s.ghgRefCO2                 =   1000; %ktCO2eq/MtCO2
            g.landMax                   =   10^6.*linspace(1,0,s.runTime);   %ha
end

    %Choose weather year - current data includes 2016-18 (year 1-3)
    s.weatherYear                   =   3;
        
    %Calculation of hourly residual load and other power metrics
    [residualLoad,powerYear,s.RENshare,s.RENshare100]   =   vrePower(powerData,s.onShore,s.offShore,s.photoV,s.demandPower,s.MustRun,s.RENMustRun,s.powerStorage,s.powerStorageMax);
    
    %Calculation of surplus power (TWh) per year and time step
    [s.surplusPowerVar,s.surplusPowerVar2,s.posResLoad,s.resLoadDataHour]   =   surplusPower(g,s,residualLoad,powerYear);
    
    
    %Setting dispatchable power demand as upper limit in power sector
    g.Demand(4,:)                   =   sum(s.posResLoad,1).*3.6; 
    
    
    %Set demand in aviation, freight (goods) and maritime (ship) sectors
    s.demAviation                   =   linspace(300,400,s.runTime);
    s.demGoodsLand                  =   linspace(555,555*0.5,s.runTime);
    s.demShip                       =   linspace(589,589*0.7,s.runTime);
    
    %All demands in one matrix
    g.Demand                        =   [g.Demand; %demand from setData
                                        s.demLand;
                                        s.demAviation;
                                        s.demGoodsLand;
                                        s.demShip;
                                        3000.*ones(s.runTime,1)';%marketCH4 - "unlimited"
                                        3000.*ones(s.runTime,1)'];%marketH2 - "unlimited"
        
    % Call function for feed cost development
    [s,f]                           =   feedCost(s,f);
    
    % OPEX & CAPEX calculation
    [o(scenario),p(scenario)]       =   costDevNoLearning(s,f); 
  
    % Define GAMS parameters
    [gamsTemp,g]                    =   gamsVar(s,f,o(scenario),p(scenario),scenario,g,techData);
    
    % Run GAMS
    [gamsOut(scenario),g]           =   gamsRun(s,f,g,scenario,noScenarios,gamsTemp,'ghgMax');

%% Iterate pareto
noPareto = 2;

% Set the GHG target vector (as share of max. achievable GHG abatement from GHG
% optimization)
iterStep =  [0.99,0.95,0.85,0.7,0.5,0.6];

for paretoIter=1:noPareto
    gamsTemp.ghgTarget.val          =   iterStep(paretoIter)*gamsOut(scenario).ghgTarget;
    
    [paretoTemp,g]                  =   gamsRun(s,f,g,scenario,noScenarios,gamsTemp,'costMin');
    paretoVar(scenario,paretoIter)  =   paretoTemp;
end
end

%% Plotting
plotting(s,f,o,p,g,gamsOut,noScenarios,paretoVar,noPareto,iterStep)

